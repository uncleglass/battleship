package pl.uncleglass.battleship.game;

import java.util.stream.IntStream;

class BoardPrinter {
    private BoardPrinter() {
    }

    public static String print(Board board) {
        var stringBuilder = new StringBuilder();
        stringBuilder
                .append("  ")
                .append(generateColumnHeaders())
                .append("\n");
        var rowHeader = 'A';
        for (var row = 1; row <= 10; row++) {
            stringBuilder
                    .append(rowHeader).append(" ")
                    .append(generateRow(row, board))
                    .append("\n");
            rowHeader++;
        }
        return stringBuilder.toString();
    }

    private static String generateColumnHeaders() {
        var stringBuilder = new StringBuilder();
        IntStream.range(1, 11)
                .forEach(i -> stringBuilder.append(i).append(" "));
        return stringBuilder.toString();
    }

    private static String generateRow(int row, Board board) {
        var stringBuilder = new StringBuilder();
        for (var column = 1; column <= 10; column++) {
            var c = fieldToChar( Coordinate.of(row, column), board);
            stringBuilder.append(c).append(" ");
        }
        return stringBuilder.toString();
    }

    private static char fieldToChar(Coordinate coordinate, Board board) {
        if (board.isFieldHit(coordinate)) {
            return 'X';
        }
        if (board.isFieldMiss(coordinate)) {
            return '*';
        }
        if (board.isFieldShip(coordinate)) {
            return 'O';
        }
        return '~';
    }

    public static String print(Board myBoard, Board opponentBoard) {
        var stringBuilder = new StringBuilder();
        stringBuilder
                .append("  my board                 opponent board\n")
                .append("  ")
                .append(generateColumnHeaders())
                .append("    ")
                .append(generateColumnHeaders())
                .append("\n");
        var rowHeader = 'A';
        for (var row = 1; row <= 10; row++) {
            stringBuilder
                    .append(rowHeader).append(" ")
                    .append(generateRow(row, myBoard))
                    .append("   ").append(rowHeader).append(" ")
                    .append(generateRow(row, opponentBoard))
                    .append("\n");
            rowHeader++;
        }
        return stringBuilder.toString();
    }
}
