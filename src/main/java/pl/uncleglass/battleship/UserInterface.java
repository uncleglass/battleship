package pl.uncleglass.battleship;

import java.util.Scanner;

public class UserInterface {
    private static final Scanner scanner = new Scanner(System.in);

    private UserInterface() {
    }

    public static void print(String string) {
        System.out.println(string);
    }

    public static String read() {
        return scanner.nextLine();
    }

    public static String read(String string) {
        print(string);
        return scanner.nextLine();
    }
}
