package pl.uncleglass.battleship.modes.server;

import pl.uncleglass.battleship.game.Game;
import pl.uncleglass.battleship.modes.shared.request.Request;
import pl.uncleglass.battleship.modes.shared.response.Response;

class ShotRequestRequestCommand extends Command {

    protected ShotRequestRequestCommand(Request request) {
        super(request);
    }

    @Override
    public Response execute(Game game) {
        var shot = game.getShot();
        return Response.shotRequest(shot);
    }
}
