package pl.uncleglass.battleship;

import pl.uncleglass.battleship.modes.ModeFactory;

public class Starter {
    private static final String BANNER = """
              _               _     _     _                _       _
             | |             | |   | |   | |              | |     (_)
             | |__     __ _  | |_  | |_  | |   ___   ___  | |__    _   _ __
             |  _ \\   / _  | | __| | __| | |  / _ \\ / __| |  _ \\  | | |  _ \\
             | |_) | | (_| | | |_  | |_  | | |  __/ \\__ \\ | | | | | | | |_) |
             |____/   \\____|  \\__|  \\__| |_|  \\___| |___/ |_| |_| |_| | ___/
                                                                      | |
                                                                      |_|                                                                                                                                                                        \s
            """;
    private static final String CHOOSE_MODE_MENU = """
            Choose mode:
            [1] - Manual client
            [2] - AI client
            [3] - Server
            [4] - Exit""";

    public void start() {
        UserInterface.print(BANNER);
        var run = true;
        while (run) {
            UserInterface.print(CHOOSE_MODE_MENU);
            var choice = UserInterface.read();
            var mode = ModeFactory.get(choice);
            run = mode.run();
        }
    }
}
