package pl.uncleglass.battleship.modes.client;

import pl.uncleglass.battleship.UserInterface;
import pl.uncleglass.battleship.game.Game;
import pl.uncleglass.battleship.modes.shared.request.Request;
import pl.uncleglass.battleship.modes.shared.response.Response;

class GameInvitationResponseCommand extends Command {
    public GameInvitationResponseCommand(Response response) {
        super(response);
    }

    @Override
    public Request execute(Game game) {
        game.prepareGame();
        UserInterface.print("Shooting");
        var boards = game.getBoardsPrint();
        UserInterface.print(boards);
        var shot = game.getShot();
        UserInterface.print("My shot: " + shot);
        return Request.shot(shot);
    }
}
