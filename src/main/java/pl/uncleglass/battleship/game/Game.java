package pl.uncleglass.battleship.game;

import pl.uncleglass.battleship.dtos.BoardDto;
import pl.uncleglass.battleship.dtos.ShotDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Game {
    private Board myBoard;
    private Board opponentBoard;
    private Fleet myFleet;
    private final ShotDecisionProvider shotDecisionProvider;
    private boolean continued = true;
    private Shots myShots;
    private Shots opponentShots;
    private BoardDto boardDto;
    private long gameId;

    Game(ShotDecisionProvider shotDecisionProvider) {
        this.shotDecisionProvider = shotDecisionProvider;
    }

    public void prepareGame() {
        myBoard = new Board();
        var boardFiller = new BoardFiller(myBoard);
        boardFiller.fill();

        myFleet = Fleet.create(myBoard);
        boardDto = generateBoardFilling();

        opponentBoard = new Board();

        myShots = new Shots();
        opponentShots = new Shots();
    }

    private BoardDto generateBoardFilling() {
        var four = "";
        List<String> three = new ArrayList<>();
        List<String> two = new ArrayList<>();
        List<String> one = new ArrayList<>();
        for (Ship ship : myFleet.getShips()) {
            int size = ship.getCoordinates().size();
            if (size == 4) {
                four = convertShip(ship);
            }
            if (size == 3) {
                three.add(convertShip(ship));
            }
            if (size == 2) {
                two.add(convertShip(ship));
            }
            if (size == 1) {
                one.add(convertShip(ship));
            }
        }
        return new BoardDto(four, three, two, one);
    }

    private String convertShip(Ship ship) {
        List<Coordinate> coordinates = new ArrayList<>(ship.getCoordinates());
        if (coordinates.size() == 1) {
            var shotDto = Coordinate.toShot(coordinates.get(0));
            return shotDto.toString();
        }
        coordinates.sort((c1, c2) -> {
            if (c1.getRow() - c2.getRow() == 0) {
                return Integer.compare(c1.getColumn(), c2.getColumn());
            } else {
                return Integer.compare(c1.getRow(), c2.getRow());
            }
        });
        var head = coordinates.get(0);
        var tail = coordinates.get(coordinates.size() - 1);

        return Coordinate.toShot(head) + "-" + Coordinate.toShot(tail);
    }

    public boolean isContinued() {
        return continued;
    }

    public void stop() {
        continued = false;
    }

    public boolean whetherIWon() {
        return opponentBoard.hitsCount() == 20;
    }

    public boolean whetherOpponentWon() {
        return myFleet.size() == 0;
    }

    public ShotDto getShot() {
        var nextShot = shotDecisionProvider.nextShot(myShots, opponentBoard);
        myShots.setActual(nextShot);
        return Coordinate.toShot(nextShot);
    }

    public String checkShot(ShotDto shotDto) {
        var coordinate = Coordinate.from(shotDto);
        opponentShots.setActual(coordinate);
        if (myBoard.isFieldEmpty(coordinate)) {
            myBoard.setMiss(coordinate);
            opponentShots.setMissAsResult();
            return Result.MISS.toString();
        }
        if (myBoard.isFieldMiss(coordinate)) {
            return Result.MISS.toString();
        }
        if (myBoard.isFieldHit(coordinate)) {
            return Result.HIT.toString();
        }
        if (myBoard.isFieldShip(coordinate)) {
            myBoard.setHit(coordinate);
            var sizeBeforeUpdate = myFleet.size();
            myFleet.removeSunkenShips(myBoard);
            var sizeAfterUpdate = myFleet.size();
            if (sizeAfterUpdate == sizeBeforeUpdate) {
                opponentShots.setHitAsResult();
                return Result.HIT.toString();
            } else {
                opponentShots.setSinkingAsResult();
                return Result.SINKING.toString();
            }
        }
        return null;
    }

    public void setHit() {
        myShots.setHitAsResult();
        opponentBoard.setHit(myShots.actual());
    }

    public void setMiss() {
        myShots.setMissAsResult();
        opponentBoard.setMiss(myShots.actual());
    }

    public void setSinking() {
        myShots.setSinkingAsResult();
        opponentBoard.setHit(myShots.actual());
        opponentBoard.updateReserved();
    }

    public boolean isOpponentLastShotAHit() {
        return opponentShots.isTheLastShotAHit() || opponentShots.isTheLastShotASinking();
    }

    public BoardDto getBoardFilling() {
        return boardDto;
    }

    public String getBoardsPrint() {
        return BoardPrinter.print(myBoard, opponentBoard);
    }

    public void setGameId(long gameId) {
        this.gameId = gameId;
    }

    public long getGameId() {
        return gameId;
    }

    public ShotDto getActualShot() {
        return Coordinate.toShot(myShots.getActualShot());
    }
}
