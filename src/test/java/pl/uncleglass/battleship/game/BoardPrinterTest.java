package pl.uncleglass.battleship.game;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class BoardPrinterTest implements SampleBoard {

    @Test
    @DisplayName("Should print board")
    void shouldPrintBoard() {
        Board board = new Board();

        String print = BoardPrinter.print(board);

        String printOfBoard = """  
                  1 2 3 4 5 6 7 8 9 10\s
                A ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\s
                B ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\s
                C ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\s
                D ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\s
                E ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\s
                F ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\s
                G ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\s
                H ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\s
                I ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\s
                J ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\s
                """;
        assertThat(print).isEqualTo(printOfBoard);
    }

    @Test
    @DisplayName("Should print two boards")
    void shouldPrintTwoBoards() {
        Board board = aBoardWithMiss("D4");
        Board board1 = aBoardWithShip("D4");

        String print = BoardPrinter.print(board, board1);

        String printOfTwoBoards = """
                  my board                 opponent board
                  1 2 3 4 5 6 7 8 9 10     1 2 3 4 5 6 7 8 9 10\s
                A ~ ~ ~ ~ ~ ~ ~ ~ ~ ~    A ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\s
                B ~ ~ ~ ~ ~ ~ ~ ~ ~ ~    B ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\s
                C ~ ~ ~ ~ ~ ~ ~ ~ ~ ~    C ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\s
                D ~ ~ ~ * ~ ~ ~ ~ ~ ~    D ~ ~ ~ O ~ ~ ~ ~ ~ ~\s
                E ~ ~ ~ ~ ~ ~ ~ ~ ~ ~    E ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\s
                F ~ ~ ~ ~ ~ ~ ~ ~ ~ ~    F ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\s
                G ~ ~ ~ ~ ~ ~ ~ ~ ~ ~    G ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\s
                H ~ ~ ~ ~ ~ ~ ~ ~ ~ ~    H ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\s
                I ~ ~ ~ ~ ~ ~ ~ ~ ~ ~    I ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\s
                J ~ ~ ~ ~ ~ ~ ~ ~ ~ ~    J ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\s
                """;
        assertThat(print).isEqualTo(printOfTwoBoards);

    }
}