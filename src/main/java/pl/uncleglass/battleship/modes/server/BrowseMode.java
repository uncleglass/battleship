package pl.uncleglass.battleship.modes.server;

import pl.uncleglass.battleship.UserInterface;
import pl.uncleglass.battleship.modes.Mode;
import pl.uncleglass.battleship.modes.exit.ExitMode;

class BrowseMode implements Mode {
    private static final String MENU = """
            Choose what you want to do:
            [1] - Display game
            [2] - Delete game
            [3] - Back
            [4] - Exit
            """;

    @Override
    public boolean run() {
        var run = true;
        while (run) {
            UserInterface.print(MENU);
            var choice = UserInterface.read();
            var mode = BrowseModeFactory.get(choice);
            if (mode instanceof ExitMode) {
                return false;
            }
            run = mode.run();
        }
        return true;
    }
}
