package pl.uncleglass.battleship.modes.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.uncleglass.battleship.dtos.ShotDto;
import pl.uncleglass.battleship.game.Game;
import pl.uncleglass.battleship.modes.shared.JsonConverter;
import pl.uncleglass.battleship.modes.shared.request.Request;
import pl.uncleglass.battleship.modes.shared.response.Response;

class ShotRequestCommand extends Command {
    private static final Logger logger = LogManager.getLogger(ShotRequestCommand.class);
    protected ShotRequestCommand(Request request) {
        super(request);
    }

    @Override
    public Response execute(Game game) {
        var body = request.getBody().toString();
        var shot = JsonConverter.from(body, ShotDto.class);
        if (shot.verify()) {
            var result = game.checkShot(shot);
            if ("SINKING".equals(result) && game.whetherOpponentWon()) {
                    logger.info("Client won the game");
            }
            DataManager dataManager = new DataManager();
            dataManager.saveClientShot(shot, result, game.getGameId());
            return Response.shot(result);
        } else {
            logger.error("Incorrect shot: {}", shot);
            game.stop();
            logger.info("The game will be terminated");
            return Response.badShotRequest(shot);
        }
    }
}
