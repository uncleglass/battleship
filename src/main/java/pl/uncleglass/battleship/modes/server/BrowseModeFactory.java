package pl.uncleglass.battleship.modes.server;

import pl.uncleglass.battleship.modes.Mode;
import pl.uncleglass.battleship.modes.error.ErrorMode;
import pl.uncleglass.battleship.modes.exit.BackMode;
import pl.uncleglass.battleship.modes.exit.ExitMode;

class BrowseModeFactory {
    private BrowseModeFactory() {
    }

    public static Mode get(String choice) {
        return switch (choice) {
            case "1" -> new DisplayMode();
            case "2" -> new DeleteMode();
            case "3" -> new BackMode();
            case "4" -> new ExitMode();
            default -> new ErrorMode();
        };
    }
}
