package pl.uncleglass.battleship.game;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class ProbabilityMatrixTest implements SampleBoard, SampleShip {

    private ProbabilityMatrix matrix;

    @BeforeEach
    void setUp() {
        matrix = new ProbabilityMatrix();
    }

    @Test
    @DisplayName("Should return empty list")
    void shouldReturnEmptyList() {
        matrix.count(Set.of());

        List<Coordinate> coordinates = matrix.getCoordinatesWithHighestProbability();

        assertThat(coordinates.size()).isZero();
    }

    @Test
    @DisplayName("Should return list with sixteen elements")
    void shouldReturnListWithSixteenElements() {
        Set<Ship> ships = ShipGenerator.generate(4, emptyBoard());
        matrix.count(ships);

        List<Coordinate> coordinates = matrix.getCoordinatesWithHighestProbability();

        assertThat(coordinates.size()).isEqualTo(16);
    }

    @Test
    @DisplayName("Should return list with one hundred elements")
    void shouldReturnListWithOneHundredElements() {
        Set<Ship> ships = ShipGenerator.generate(1, emptyBoard());
        matrix.count(ships);

        List<Coordinate> coordinates = matrix.getCoordinatesWithHighestProbability();

        assertThat(coordinates.size()).isEqualTo(100);
    }

    @Test
    @DisplayName("Should return list with one element")
    void shouldReturnListWithOneElement() {
        Set<Ship> ships = ShipGenerator.generate(4, aBoardWithShips());
        matrix.count(ships);

        List<Coordinate> coordinates = matrix.getCoordinatesWithHighestProbability();

        assertThat(coordinates.size()).isOne();
    }

    @Test
    @DisplayName("Should return list with two elements")
    void shouldReturnListWithTwoElements() {
        Set<Ship> ships = ShipGenerator.generate(2, aBoardWithShips());
        matrix.count(ships);

        List<Coordinate> coordinates = matrix.getCoordinatesWithHighestProbability();

        assertThat(coordinates.size()).isEqualTo(2);
    }
}