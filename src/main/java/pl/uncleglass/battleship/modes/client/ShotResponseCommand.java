package pl.uncleglass.battleship.modes.client;

import pl.uncleglass.battleship.UserInterface;
import pl.uncleglass.battleship.game.Game;
import pl.uncleglass.battleship.modes.shared.JsonConverter;
import pl.uncleglass.battleship.modes.shared.request.Request;
import pl.uncleglass.battleship.modes.shared.response.Response;
import pl.uncleglass.battleship.modes.shared.response.Status;

class ShotResponseCommand extends Command {
    public ShotResponseCommand(Response response) {
        super(response);
    }

    @Override
    public Request execute(Game game) {
        int statusCode = response.getStatus();
        if (statusCode == 0) {
            UserInterface.print("Receiving shot result");
            var body = response.getBody().toString();
            var result = JsonConverter.from(body, String.class);
            UserInterface.print("My shot result: " + result);
            if (result.equals("MISS")) {
                game.setMiss();
                UserInterface.print("Shot Request was sent");
                return Request.shotRequest();
            }
            if (result.equals("HIT")) {
                game.setHit();
                UserInterface.print("Shooting");
                var boards = game.getBoardsPrint();
                UserInterface.print(boards);
                var shot = game.getShot();
                UserInterface.print("My shot: " + shot);
                return Request.shot(shot);
            }
            if (result.equals("SINKING")) {
                game.setSinking();
                if (game.whetherIWon()) {
                    UserInterface.print("Game over");
                    UserInterface.print("You won the game");
                    return Request.board(game.getBoardFilling());
                }
                UserInterface.print("Shooting");
                var boards = game.getBoardsPrint();
                UserInterface.print(boards);
                var shot = game.getShot();
                UserInterface.print("My shot: " + shot);
                return Request.shot(shot);
            }
        } else {
            UserInterface.print("Error occurred: " + Status.of(statusCode));
            game.stop();
        }
        return Request.unknown();
    }
}
