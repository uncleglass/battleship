package pl.uncleglass.battleship.game;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class BoardFillerTest {

    @Test
    @DisplayName("Should fill a board")
    void shouldFillABoard() {
        Board board = new Board();
        BoardFiller boardFiller = new BoardFiller(board);

        boardFiller.fill();

        assertThat(board.getShipsCoordinates().size()).isEqualTo(20);
    }
}