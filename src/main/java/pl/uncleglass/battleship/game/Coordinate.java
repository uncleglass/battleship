package pl.uncleglass.battleship.game;

import pl.uncleglass.battleship.dtos.ShotDto;

import java.util.Objects;

class Coordinate {
    private final int row;
    private final int column;

    private Coordinate(int row, int column) {
        if (isOutOfTheRange(row) || isOutOfTheRange(column)) {
            throw new IllegalArgumentException("Invalid coordinate");
        }
        this.row = row;
        this.column = column;
    }

    public static Coordinate of(int row, int column) {
        return new Coordinate(row, column);
    }

    public static Coordinate from(ShotDto shotDto) {
        return new Coordinate(charRowToIntValue(shotDto.getRow()), shotDto.getColumn());
    }

    private static boolean isOutOfTheRange(int i) {
        return i < 1 || i > Board.BOARD_SIZE;
    }

    public static boolean isInTheRange(int i) {
        return !isOutOfTheRange(i);
    }

    public static int charRowToIntValue(char row) {
        return row - 64;
    }

    public static Coordinate from(String s) {
        char[] chars = s.toCharArray();
        var r = Character.toUpperCase(chars[0]);
        var row = charRowToIntValue(r);
        var col = Character.getNumericValue(chars[1]);
        if (chars.length == 3) {
            var c = s.substring(1);
            return new Coordinate(row, Integer.parseInt(c));
        }
        return new Coordinate(row, col);
    }

    public static ShotDto toShot(Coordinate coordinate) {
        return ShotDto.of(
                (char) (coordinate.getRow() + 64),
                coordinate.getColumn()
        );
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public boolean isNeighbour(Coordinate coordinate) {
        return row == coordinate.row - 1 && column == coordinate.column ||
                row == coordinate.row + 1 && column == coordinate.column ||
                row == coordinate.row && column == coordinate.column - 1 ||
                row == coordinate.row && column == coordinate.column + 1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return row == that.row && column == that.column;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, column);
    }

    @Override
    public String toString() {
        return "(" + row + ", " + column + ")";
    }
}
