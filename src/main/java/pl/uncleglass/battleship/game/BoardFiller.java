package pl.uncleglass.battleship.game;

import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

class BoardFiller {
    private final Board board;
    private final Random random = new Random();

    public BoardFiller(Board board) {
        this.board = board;
    }

    public void fill() {
        setFourMastShip();
        setThreeMastShips();
        setTwoMastShips();
        setOneMastShips();
    }

    private void setFourMastShip() {
        setShip(4);
    }

    private void setThreeMastShips() {
        setThreeMast();
        setThreeMast();
    }

    private void setTwoMastShips() {
        setTwoMast();
        setTwoMast();
        setTwoMast();
    }

    private void setOneMastShips() {
        setOneMast();
        setOneMast();
        setOneMast();
        setOneMast();
    }

    private void setThreeMast() {
        setShip(3);
    }

    private void setTwoMast() {
        setShip(2);
    }

    private void setOneMast() {
        setShip(1);
    }

    private void setShip(int shipSize) {
        Set<Ship> ships = ShipGenerator.generate(shipSize, board);
        var ship = getRandomShip(ships);
        board.setShip(ship);
    }

    private Ship getRandomShip(Set<Ship> ships) {
        ArrayList<Ship> shipArrayList = new ArrayList<>(ships);
        var randomIndex = random.nextInt(shipArrayList.size());
        return shipArrayList.get(randomIndex);
    }
}
