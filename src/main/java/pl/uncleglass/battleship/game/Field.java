package pl.uncleglass.battleship.game;

class Field {
    private enum OccupationType {
        EMPTY, SHIP, HIT, MISS
    }

    private OccupationType occupation = OccupationType.EMPTY;

    void ship() {
        occupation = OccupationType.SHIP;
    }

    void hit() {
        occupation = OccupationType.HIT;
    }

    void miss() {
        occupation = OccupationType.MISS;
    }

    boolean isEmpty() {
        return occupation == OccupationType.EMPTY;
    }

    boolean isShip() {
        return occupation == OccupationType.SHIP;
    }

    boolean isHit() {
        return occupation == OccupationType.HIT;
    }

    boolean isMiss() {
        return occupation == OccupationType.MISS;
    }
}

