package pl.uncleglass.battleship.modes.shared.response;

import pl.uncleglass.battleship.dtos.ShotDto;
import pl.uncleglass.battleship.modes.shared.request.RequestType;

public class Response {
    private final RequestType type;
    private final int status;
    private final String message;
    private final Object body;

    private Response(RequestType type, Status status, String message, Object body) {
        this.type = type;
        this.status = status.getValue();
        this.message = message;
        this.body = body;
    }

    public static Response badRequest() {
        return new Response(RequestType.UNKNOWN,
                Status.BAD_REQUEST,
                "Cannot recognise request type.",
                null);
    }

    public static Response badShotRequest(ShotDto shot) {
        return new Response(
                RequestType.SHOT,
                Status.ILLEGAL_ARGUMENTS,
                "Received shot is incorrect: " + shot,
                null
        );

    }

    public static Response gameInvitation(Status status, String message) {
        return new Response(RequestType.GAME_INVITATION, status, message, null);
    }

    public static Response shot(Object body) {
        return new Response(RequestType.SHOT, Status.OK, null, body);
    }

    public static Response shot(Status status, String message) {
        return new Response(RequestType.SHOT, status, message, null);
    }

    public static Response shotRequest(Object body) {
        return new Response(RequestType.SHOT_REQUEST, Status.OK, null, body);
    }

    public static Response result() {
        return new Response(RequestType.RESULT, Status.OK, null, null);
    }

    public static Response board() {
        return new Response(RequestType.BOARD, Status.OK, null, null);
    }

    public RequestType getType() {
        return type;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Object getBody() {
        return body;
    }
}
