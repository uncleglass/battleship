package pl.uncleglass.battleship.modes.server;

import pl.uncleglass.battleship.modes.shared.request.Request;

class CommandFactory {
    private CommandFactory() {
    }

    public static Command get(Request request) {
        if (request.isNull()) {
            return new BadRequestCommand(request);
        }
        return switch (request.getType()) {
            case GAME_INVITATION -> new GameInvitationRequestCommand(request);
            case SHOT -> new ShotRequestCommand(request);
            case SHOT_REQUEST -> new ShotRequestRequestCommand(request);
            case RESULT -> new ResultRequestCommand(request);
            case BOARD -> new BoardRequestCommand(request);
            default -> new BadRequestCommand(request);
        };
    }
}
