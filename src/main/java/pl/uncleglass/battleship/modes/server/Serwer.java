package pl.uncleglass.battleship.modes.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.uncleglass.battleship.game.Game;
import pl.uncleglass.battleship.game.GameFactory;
import pl.uncleglass.battleship.modes.Mode;
import pl.uncleglass.battleship.modes.shared.Connection;
import pl.uncleglass.battleship.modes.shared.JsonConverter;
import pl.uncleglass.battleship.modes.shared.request.Request;
import pl.uncleglass.battleship.modes.shared.response.Response;

import java.io.IOException;
import java.net.ServerSocket;

class Serwer implements Mode {
    private static final Logger logger = LogManager.getLogger(Serwer.class);
    public static final int PORT = 6666;
    Connection client;
    Game game = GameFactory.aiGame();

    @Override
    public boolean run() {
        //TODO console info - game started

        client = establishConnection();
        var clientName = client.getName();
        DataManager dataManager = new DataManager();
        long gameId = dataManager.saveGame(clientName);
        game.setGameId(gameId);

        while (client.isConnected() && game.isContinued()) {
            var request = getRequest();
            var command = CommandFactory.get(request);
            var response = command.execute(game);
            sendResponse(response);
        }
        client.close();
        return true;
    }

    private Connection establishConnection() {
        Connection connection = null;
        try (var serverSocket = new ServerSocket(PORT)) {
            var accept = serverSocket.accept();
            connection = new Connection(accept);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private Request getRequest() {
        var read = client.readAndCheckIfConnected();
        logger.info("Received message: {}", read);
        return JsonConverter.from(read, Request.class);
    }

    private void sendResponse(Response response) {
        var to = JsonConverter.to(response);
        logger.info("Sent message: {}", to);
        client.write(to);
    }
}
