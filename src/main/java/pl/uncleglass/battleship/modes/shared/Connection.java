package pl.uncleglass.battleship.modes.shared;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Connection {
    private final Socket socket;
    private final PrintWriter out;
    private final BufferedReader in;
    private boolean connected;

    public Connection(Socket socket) throws IOException {
        this.socket = socket;
        out = new PrintWriter(this.socket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
        connected = true;
    }

    public void write(String message) {
        out.println(message);
    }

    public String readAndCheckIfConnected() {
        try {
            String line = in.readLine();
            if (line == null) {
                connected = false;
                return "";
            }
            return line;
        } catch (IOException e) {
            connected = false;
            return "";
        }
    }

    public boolean isConnected() {
        return connected;
    }

    public void close() {
        try {
            out.close();
            in.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getName() {
        String s = socket.toString();
        s = s.replace("Socket[", "").replace("]", "");
        String[] split = s.split(",");
        return split[0] + " " + split[1];
    }
}
