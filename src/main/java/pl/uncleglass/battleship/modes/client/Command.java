package pl.uncleglass.battleship.modes.client;

import pl.uncleglass.battleship.game.Game;
import pl.uncleglass.battleship.modes.shared.request.Request;
import pl.uncleglass.battleship.modes.shared.response.Response;

abstract class Command {
    protected final Response response;

    protected Command(Response response) {
        this.response = response;
    }

    public abstract Request execute(Game game);
}
