package pl.uncleglass.battleship.modes.shared.request;

import pl.uncleglass.battleship.dtos.BoardDto;
import pl.uncleglass.battleship.dtos.ShotDto;

public class Request {
    private final RequestType type;
    private final Object body;

    private Request(RequestType type, Object body) {
        this.type = type;
        this.body = body;
    }

    public static Request gameInvitation() {
        return new Request(RequestType.GAME_INVITATION, null);
    }

    public static Request shot(ShotDto shotDto) {
        return new Request(RequestType.SHOT, shotDto);
    }

    public static Request shotRequest() {
        return new Request(RequestType.SHOT_REQUEST, null);
    }

    public static Request result(String result) {
        return new Request(RequestType.RESULT, result);
    }

    public static Request board(BoardDto boardDto) {
        return new Request(RequestType.BOARD, boardDto);
    }

    public static Request unknown() {
        return new Request(RequestType.UNKNOWN, null);
    }

    public RequestType getType() {
        return type;
    }

    public Object getBody() {
        return body;
    }

    public boolean isNull() {
        return type == null;
    }
}
