package pl.uncleglass.battleship.modes.client;

import pl.uncleglass.battleship.UserInterface;
import pl.uncleglass.battleship.dtos.ShotDto;
import pl.uncleglass.battleship.game.Game;
import pl.uncleglass.battleship.modes.shared.JsonConverter;
import pl.uncleglass.battleship.modes.shared.request.Request;
import pl.uncleglass.battleship.modes.shared.response.Response;

class ShotRequestResponseCommand extends Command {
    public ShotRequestResponseCommand(Response response) {
        super(response);
    }

    @Override
    public Request execute(Game game) {
        UserInterface.print("Checking opponent shot");
        var body = response.getBody().toString();
        var shot = JsonConverter.from(body, ShotDto.class);
        UserInterface.print("Opponent shot: " + shot);
        var boards = game.getBoardsPrint();
        UserInterface.print(boards);
        var result = game.checkShot(shot);
        UserInterface.print("Shot result: " + result);
        return Request.result(result);
    }
}
