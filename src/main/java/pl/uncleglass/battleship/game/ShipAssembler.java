package pl.uncleglass.battleship.game;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

class ShipAssembler {
    private ShipAssembler() {
    }

    public static Set<Ship> assembly(List<Coordinate> coordinates) {
        Set<Ship> assembledShips = new HashSet<>();
        while (!coordinates.isEmpty()) {
            var shipCoordinate = coordinates.remove(0);
            var ship = assemblyShip(shipCoordinate, coordinates);
            assembledShips.add(ship);
        }
        return assembledShips;
    }

    private static Ship assemblyShip(Coordinate shipCoordinate, List<Coordinate> coordinates) {
        ArrayDeque<Coordinate> queue = new ArrayDeque<>(Collections.singletonList(shipCoordinate));
        Set<Coordinate> assembledShipCoordinates = new HashSet<>();
        while (queue.iterator().hasNext()) {
            var coordinate = queue.poll();
            assembledShipCoordinates.add(coordinate);
            Iterator<Coordinate> iterator = coordinates.iterator();
            while (iterator.hasNext()) {
                var next = iterator.next();
                if (next.isNeighbour(coordinate)) {
                    queue.add(next);
                    iterator.remove();
                }
            }
        }
        return new Ship(assembledShipCoordinates);
    }
}