package pl.uncleglass.battleship.modes.exit;

public class ExitMode extends BackMode {
    @Override
    public boolean run() {
        return false;
    }
}
