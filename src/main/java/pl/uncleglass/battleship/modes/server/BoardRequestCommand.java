package pl.uncleglass.battleship.modes.server;

import pl.uncleglass.battleship.dtos.BoardDto;
import pl.uncleglass.battleship.game.Game;
import pl.uncleglass.battleship.modes.shared.JsonConverter;
import pl.uncleglass.battleship.modes.shared.request.Request;
import pl.uncleglass.battleship.modes.shared.response.Response;

class BoardRequestCommand extends Command {

    protected BoardRequestCommand(Request request) {
        super(request);
    }

    @Override
    public Response execute(Game game) {
        var body = request.getBody().toString();
        BoardDto boardDto = JsonConverter.from(body, BoardDto.class);
        String board = JsonConverter.to(boardDto);
        DataManager dataManager = new DataManager();
        dataManager.saveClientBoard(board, game.getGameId());
        game.stop();
        return Response.board();
    }
}
