package pl.uncleglass.battleship.game;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CoordinateTest implements SampleCoordinate {

    @ParameterizedTest
    @MethodSource("neighbours")
    @DisplayName("Should return true when coordinate is neighbour")
    void shouldReturnTrueWhenCoordinateIsNeighbour(String c) {
        Coordinate coordinate = aCoordinate("E5");

        assertTrue(coordinate.isNeighbour(aCoordinate(c)));
    }

    public static Stream<Arguments> neighbours() {
        return Stream.of(
                Arguments.of("D5"),
                Arguments.of("F5"),
                Arguments.of("E4"),
                Arguments.of("E6")
        );
    }

    @ParameterizedTest
    @MethodSource("notNeighbours")
    @DisplayName("Should return true when coordinate is not neighbour")
    void shouldReturnFalseWhenCoordinateIsNotNeighbour(String c) {
        Coordinate coordinate = aCoordinate("E5");

        assertFalse(coordinate.isNeighbour(aCoordinate(c)));
    }

    public static Stream<Arguments> notNeighbours() {
        return Stream.of(
                Arguments.of("D4"),
                Arguments.of("A1"),
                Arguments.of("G6"),
                Arguments.of("F4"),
                Arguments.of("D6"),
                Arguments.of("F6")
        );
    }

    @Test
    @DisplayName("Should not throw exception for correct parameters")
    void shouldNotThrowExceptionForCorrectParameters() {
        assertThatNoException().isThrownBy(
                () -> Coordinate.of(1, 4)
        );
    }

    @ParameterizedTest
    @MethodSource("outOfTheRange")
    @DisplayName("Should trow exception when row is out of the range")
    void shouldTrowExceptionWhenRowIsOutOfTheRange(int param) {
        Throwable throwable = catchThrowable(
                () -> Coordinate.of(param, 4)
        );

        assertThat(throwable)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Invalid coordinate");
    }

    @ParameterizedTest
    @MethodSource("outOfTheRange")
    @DisplayName("Should trow exception when column is out of the range")
    void shouldTrowExceptionWhenColumnIsOutOfTheRange(int param) {
        Throwable throwable = catchThrowable(
                () -> Coordinate.of(3, param)
        );

        assertThat(throwable)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Invalid coordinate");
    }

    static Stream<Arguments> outOfTheRange() {
        return Stream.of(
                Arguments.of(-3),
                Arguments.of(0),
                Arguments.of(11),
                Arguments.of(132)
        );
    }
}